<?php
/**
 * Created by PhpStorm.
 * User: hortt
 * Date: 19.03.18
 * Time: 21:56
 */

//Alphabet set
// 1 - I, 5 - V, 10 - X, 50 - L, 100 - C, 500 - D, 1000 - M

$number = isset($argv[1])?$argv[1]:false;

if( !$number || !is_numeric($number)) {
	die('Wrong number'.PHP_EOL);
}


//Alphabet
$alphabet = [
	'I' => 1,
	'i' => 1,
	'V' => 5,
	'v' => 5,
	'X' => 10,
	'x' => 10,
	'L' => 50,
	'l' => 50,
	'C' => 100,
	'c' => 100,
	'D' => 500,
	'd' => 500,
	'M' => 1000,
	'm' => 1000
];


$thousands  = (int)floor($number/1000);
$hundreds   = (int)floor(($number - $thousands*1000)/100);
$decades    = (int)floor(($number - $thousands*1000 - $hundreds*100)/10);
$numbers    = (int)floor($number%10);

$roma_letter = '';

for ( $M = 0; $M < $thousands; $M++ ){
	$roma_letter .= "M";
}


/**
 * Applyes valid rule to use not more than 3 same letters in a row with addition/substraction principle
 *
 * @param $number
 * @param $bigger
 * @param $current
 * @param $middle
 *
 * @return string
 */
function formalize_roman_set( $number, $bigger, $current, $middle) {
	$roma_letter = '';
	if( $number == 4 ) {
		$roma_letter .= $current.$middle;
	} elseif ( $number == 5 ) {
		$roma_letter .= $middle;
	} elseif( $number == 9) {
		$roma_letter .= $current.$bigger;
	} elseif ( $number > 5  ) {
		$roma_letter .= $middle;
		for ( $H = 0; $H<($number-5); $H ++  ) {
			$roma_letter .= $current;
		}
	} else {
		for ( $H = 0; $H<$number; $H ++  ) {
			$roma_letter .= $current;
		}
	}
	return $roma_letter;
}

$roma_letter .= formalize_roman_set( $hundreds, 'M', 'C', 'D');
$roma_letter .= formalize_roman_set( $decades, 'C', 'X', 'L');
$roma_letter .= formalize_roman_set( $numbers, 'X', 'I', 'V');

echo $roma_letter.PHP_EOL;