# Light IT test task
since it's a console version PHP must be installed on your machine obviously 

##roma_arabic.php ##

###Usage ###
`php path_to_the_dir/roma_arabic.php MCMLXXVII` 

Script accepts lowercase/uppercase variants 

Script dies if wrong or empty Roma letter is given with related message:
![wrong input letter](http://dl4.joxi.net/drive/2018/03/20/0026/0706/1737410/10/bc51864e20.jpg "wrong input")

More info: [https://en.wikipedia.org/wiki/Roman_numerals#Roman_numeric_system](https://en.wikipedia.org/wiki/Roman_numerals#Roman_numeric_system)

##arabic_roma.php ##

###Usage ###
`php path_to_the_dir/arabic_roma.php 435345` 

dies when not numeric or empty input provided
![wrong input number](http://dl3.joxi.net/drive/2018/03/20/0026/0706/1737410/10/0101412dd6.jpg "wrong input")
