<?php
/**
 * Created by PhpStorm.
 * User: hortt
 * Date: 19.03.18
 * Time: 20:27
 * Accepts both uppercase/lowercase variants.
 * Examlpe usage: php path_to_the_dir/roma_arabic.php MCMLXXVII
 */

//Alphabet set
// 1 - I, 5 - V, 10 - X, 50 - L, 100 - C, 500 - D, 1000 - M

//MCMLXXVII = 1977

$string = isset($argv[1])?$argv[1]:false;

if( !$string ) {
	die('empty roma number'.PHP_EOL);
}


//Alphabet
$alphabet = [
	'I' => 1,
	'i' => 1,
	'V' => 5,
	'v' => 5,
	'X' => 10,
	'x' => 10,
	'L' => 50,
	'l' => 50,
	'C' => 100,
	'c' => 100,
	'D' => 500,
	'd' => 500,
	'M' => 1000,
	'm' => 1000
];

//make array from roma string
$letter_set = str_split($string);

//init arabic number
$summ  = 0;

foreach ( $letter_set as $key => $letter ) {

	if( !array_key_exists($letter, $alphabet) ) {
		echo 'Wrong Roma number: ' .$key. '=>'.$letter.' skipped'.PHP_EOL;
		die;
	}

	if( isset( $letter_set[$key+1]) && $alphabet[$letter_set[$key+1]] > $alphabet[$letter_set[$key]] ) {
		$summ -= $alphabet[$letter];
	} else {
		$summ += $alphabet[$letter];
	}
}

echo $summ.PHP_EOL;